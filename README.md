# GeauxWeisbeck4.dev Version 2.0

Ehh this was implemented poorly. I am disgusted at the efforts put into this - it's time for a do over. I can make a way sick website if I put some more effort into this. I am going to begin work on this shortly

## Potential Tech Choices
- Deno
  - [ ] Aleph.js
  - [ ] Fresh
  - [ ] Ultra.js
  - [ ] ~~Lume~~
  - [ ] Oak
- Node.js
  - [ ] Gatsby
  - [ ] Next.js
  - [ ] Nuxt
  - [ ] Svelte Kit
  - [ ] Astro
  - [ ] 11ty
  - [ ] Hugo
  - [ ] Jekyll

## Features to be implemented...

Yes I have a wish list:

- [ ] Add Supabase 
- [ ] Add my dashboard
- [ ] Add a commenting system
- [ ] Add a music dashboard with my top bangers
- [ ] Convert to JSX
- [ ] Add Documentation to site

## Star if you like my site! Thank you!

It's also totally cool if you want to use this site to make your own.

